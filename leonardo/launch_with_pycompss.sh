#/bin/bash
  
#pycompss job submit \
enqueue_compss \
        --num_nodes=2 \
        --cpus_per_node=32 \
        --worker_working_dir=/leonardo/home/userexternal/dlezzi00/ \
        --exec_time=10 \
        --lang=python \
        --debug \
        --queue=boost_usr_prod \
	--qos=boost_qos_dbg \
        /leonardo/home/userexternal/dlezzi00/workflow-registry/example_workflow/matmul/src/matmul_files.py 2 2
