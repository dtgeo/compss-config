# COMPSs Configuration files for Slurm system

This repository contains the configuration files to submit COMPSs jobs to Slurm in different clusters. Leonardo files are used here as example.

- [ ] Copy leonardo.cfg to COMPSS_INSTALLATION_PATH/compss/Runtime/scripts/queues/supercomputers/default.cfg
- [ ] Copy slurm.cfg to COMPSS_INSTALLATION_PATH/compss/Runtime/scripts/queues/queue_systems/


